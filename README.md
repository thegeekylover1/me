# me

More to come soon!

# Usage

## Resume Generation

To debug resume generation locally, you can start the latex container with `docker-compose up`, attach to the container, then run the pandoc command in our [.gitlab-ci.yml](.gitlab-ci.yml). Because of the volume mount in our [docker-compose.yml](docker-compose.yml), the generated PDF should automagically appear in your host machines file system.