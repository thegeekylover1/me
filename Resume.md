---
name: The Geeky Lover
keywords: many!
left-column:
 - 'E-mail: upon application'
 - 'Phone: upon application'
right-column:
 - 'Location: New York City'
 - 'Personal Site: [https://gitlab.com/thegeekylover1/me/](https://gitlab.com/thegeekylover1/me/)'
...

# Summary

A technologist with over 25 years of professional experience leading the architecture, design, and implementation of multiple enterprise SaaS solutions.
A leader and collaborator within organizations to achieve improved operational efficiencies through automation and transparency
A geek at heart looking to find an amazing employer that wants to maximize the efficiency, effectiveness, and quality of every engineer's work!

# Experience

## 3DS Medidata, Vice President Engineering, NYC

**2013 - present** 

Trusted technology leader focused on both the delivery of clinical trial SaaS solutions and transformation of how the organization operates

- Delivered thousands of features, across dozens of teams, for hundreds of projects every year while maintaining or improving velocity
- Partnered with every functional group to improve operational efficiencies
- Engaged with multiple top 25 pharmaceutical clients like J&J, Novartis, and Amgen

Projects of note that permanently changed how Medidata operated

- Project Management: Demonstrated and implemented automated requirements/design/implementation traceability to satisfy clinical regulations. This traceability automation has saved tens of millions of dollars in manual binder generation over 10+ years.
- Customer Success: Established a customer care to engineering integration that dramatically (5x) reduced ticket close times and improved CSAT by double digits
- Legal: Pioneered, promoted, and participated in the companies' patent program. Hundreds of patents have been issued as a result of this program to protect Medidata IP.

Projects of note that permanently change how engineers at Medidata operated

- Process: Authored dozens of standard operating procedures (SOPs/manuals) used by thousands of engineers over 10+ years. These SOPs resulted in a mean time to test/deploy a system from 12 weeks down to two weeks. Some teams have also achieved same day code merge to production deployments which is very difficult to accomplish in the heavily regulated industry of clinical trials. 
- Innovation: Created the "innovation time" program for engineers. Hundreds of projects were created and many were later put into production. This program was also a huge driver in getting Medidata on the Glassdoor's Best Places to Work list.
- Code review: Championed and implemented electronic independent peer review of all code changes. This eliminated paper processes that were "rubber stamps" and improved system quality through *real* peer review before code was merged.

## 3DS Medidata, Manager and Director of Engineering, NYC

**2009 - 2013** 

Lead multiple agile teams in the delivery of Medidata products. Process engineer that lead cross functional teams into more efficient ways of operating.

As a manager and director they

 - architected, designed, implemented, and manged from the ground up, the creation of the world's first fully configurable randomization and trial supply logistics system. This system has been used to manage thousands of clinical trials operated around the world and is one of the highest revenue generating products in the company today.
 - Fully automated the validation processes required by our regulators to achieve the company's first same day code freeze to production deployment in 2010. This automation has saved the company tens of millions over the last decade when compared to the processes in place before.
 - Directly coached, mentored and advised hundreds of engineers on the craft of software engineering. This was done in person and virtually across 10,000+ change requests. 


## ReQuest Technologies and etrials, Lead Software Engineer, Ithaca, New York and Remote

**1997 - 2009** 

When you work for a four person company, you end up wearing many (all) the hats.

- Administration of the all of the company’s digital assets including network switches, telephony equipment, system servers, etc.
- Architecture, design, and implementation of multiple SaaS applications
- Operated in many client facing roles such as customer success agent, technical trainer, sales presenter, conference presenter

# Education

## State University of New York at Potsdam

**1996 - 2000**

- BA in Computer and Information Sciences
- BA in Mathematics
- Minor in Human Communications
- Graduated Magna Cum Laude with a dozen awards and accolades for my achievements

# Technical Skills

Languages and Frameworks: Ruby/Ruby on Rails (RoR), Typescript/Angular, Python/Python Flask, PHP/Cake, C/C++, Java, Perl 
 
Tools: SCC - Git/Subversion, Unix - Ubuntu/RedHat/Solaris, Editors - VS Code/ViM/Emacs, Databases - PostgreSQL/ 
Oracle/MySQL/Redis 
 
Cloud Services (AWS/Google): Instances - ECS/PKS/EC2, Stateless - Lambda, File Storage - S3, Managed DB - RDS/Aurora, Queuing - SQS, Managed - Elastic Beanstalk, Kubernetes and Teraform, and many many more...
 
Concepts: Infrastructure as a Service (IaaS), Platform as a Service (PaaS), Software as a Service (SaaS), Blue Green Deployments, Behavior Driven Development (BDD), Test Driven Development (TDD), Continuous Integration/ 
Deployment (CI/CD), Twelve-Factor Apps
 
Industry Regulations: 21 Part 11/GxP/SOC 1/SOC 2/GDPR/PII/FISMA/HIPAA

# Personal Note
 
In my spare time I can be found doing Kaggle competitions with my wife, learning Spanish, cycling, or cooking for my family. 
